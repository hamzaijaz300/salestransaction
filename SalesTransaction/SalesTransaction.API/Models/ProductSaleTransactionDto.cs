﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadWithReact.Models
{
    public class ProductSaleTransactionDto
    {
        public int TotalQuantitySold { get; set; }
        public double TotalProductsRevenue { get; set; }
        public List<KeyValuePair<int, double>> TotalRevenuePerProduct { get; set; }
        public KeyValuePair<int, int> MostPopularProduct { get; set; }
        public KeyValuePair<string, double> HighestRevenueMonth { get; set; }
        
    }
}
