﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using FileUploadWithReact.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FileUploadWithReact.Controllers
{
    [EnableCors("Default")]
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        public FileController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public async Task<ActionResult<ProductSaleTransactionDto>> GetProductSalesTransactions()
        {
            var result = new ProductSaleTransactionDto();
            var products = new List<Product>();
            var file = Request.Form.Files[0];
            var path = _hostingEnvironment.WebRootPath;
            var uploads = Path.Combine(path, "uploads");
            if (file.Length > 0)
            {
                var filePath = Path.Combine(uploads, file.FileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
                using (var stream = new StreamReader(filePath))
                using (var csvReader = new CsvReader(stream, new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    HasHeaderRecord = true,
                    HeaderValidated = null,
                    MissingFieldFound = null
                }))
                {
                    products = csvReader.GetRecords<Product>().ToList();
                }

                result.TotalQuantitySold = products.Sum(a => a.Quantity);
                result.TotalRevenuePerProduct = products.GroupBy(a => a.ProductId)
                                                        .Select(a => new KeyValuePair<int, double>(a.Key, a.Sum(x => x.UnitPrice * x.Quantity))).ToList();
                result.MostPopularProduct = products.GroupBy(a => a.ProductId)
                                                 .Select(a => new KeyValuePair<int, int>(a.Key, a.Sum(a => a.Quantity)))
                                                 .OrderByDescending(a => a.Value)
                                                 .FirstOrDefault();
                result.TotalProductsRevenue = products.Sum(a => a.UnitPrice * a.Quantity);
                result.HighestRevenueMonth = products.GroupBy(a => new { month = a.SaleTime.Month })
                                                      .Select(a => new KeyValuePair<string, double>(a.FirstOrDefault().SaleTime.ToString("MMMM", CultureInfo.InvariantCulture), a.Sum(x => x.UnitPrice)))
                                                      .OrderByDescending(a => a.Value)
                                                      .FirstOrDefault();
            }
            return result;
        }

    }
}

