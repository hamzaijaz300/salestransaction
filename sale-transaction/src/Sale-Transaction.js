import React, { useState } from "react";
import axios from "axios";

class SaleTransaction extends React.Component {
  state = {
    data: null,
    selectedFile: null,
    isSuccess: false,
    loading: false,
  };

  onFileChange = (event) => {
    this.setState({ selectedFile: event.target.files[0] });
  };

  onFileUpload = async (e) => {
    this.setState({ loading: true });
    const formData = new FormData();
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );

    const res = await axios.post("https://localhost:44318/api/file", formData);
    this.setState({ isSuccess: true });
    this.setState({ loading: false });
    this.setState({ data: res.data });
  };

  fileData = () => {
    if (this.state.isSuccess) {
      return (
        <div>
          {this.state.loading ? (
            <div>"Loading data. Please wait"</div>
          ) : (
            <div>
              <p>
                Total Quantity Sold{" "}
                <b>{this.state.data && this.state.data.totalQuantitySold}</b>
              </p>
              <p>
                Most popular product{" "}
                <b>
                  {this.state.data && this.state.data.mostPopularProduct.Key}.
                </b>{" "}
                based on Total Quantity Sold:{" "}
                <b>
                  {this.state.data && this.state.data.mostPopularProduct.Value}
                </b>
              </p>
              <p>
                Total revenue of all products{" "}
                <b>{this.state.data && this.state.data.totalProductsRevenue}</b>
              </p>
              <p>
                Highest revenue month{" "}
                <b>
                  {" "}
                  {this.state.data && this.state.data.highestRevenueMonth.Key}
                </b>
                . Total revenue{" "}
                <b>
                  {this.state.data &&
                    Number(
                      this.state.data.highestRevenueMonth.Value.toFixed(2)
                    )}
                </b>
              </p>
              <p>Total revenue per product</p>
              <table className="table" style={{ display: "initial" }}>
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Revenue</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.data &&
                    this.state.data.totalRevenuePerProduct.map(
                      (item, index) => (
                        <tr key={index}>
                          <td>{item.Key}</td>
                          <td>{Number(item.Value.toFixed(2))}</td>
                        </tr>
                      )
                    )}
                  <tr></tr>
                </tbody>
              </table>
            </div>
          )}
        </div>
      );
    } else {
      return <div></div>;
    }
  };

  render() {
    return (
      <div>
        <h3>Sale transactions!</h3>
        <div>
          <input type="file" onChange={this.onFileChange} />
          <button onClick={this.onFileUpload}>Upload!</button>
        </div>
        {this.fileData()}
      </div>
    );
  }
}

export default SaleTransaction;
